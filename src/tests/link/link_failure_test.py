import logging
from abc import ABC

from tests.decorators import after_convergence
from tests.templates.failure_test import FailureTest


class LinkFailureTest(ABC, FailureTest):
    @after_convergence(FailureTest, priority=5)
    def make_failure(self):
        self.results['failure_time'] = self.agent.system_times()

        for interface in self.results['failed_interfaces']:
            logging.info('Destroying link %s of machine %s...' % (interface, self.results['failed_machine']))
            self.manager.exec(self.results['failed_machine'], f"ifconfig {interface} down", lab_hash=self.lab.hash)
