import logging
from abc import ABC

from tests.decorators import after_convergence
from tests.templates.recovery_test import RecoveryTest
from .link_failure_test import LinkFailureTest


class LinkRecoveryTest(RecoveryTest, LinkFailureTest, ABC):
    @after_convergence(RecoveryTest, priority=15)
    def make_recovery(self):
        self.results['recover_time'] = self.agent.system_times()

        for interface in self.results['failed_interfaces']:
            logging.info('Enabling link %s of machine %s...' % (interface, self.results['failed_machine']))
            self.manager.exec(self.results['failed_machine'], f"ifconfig {interface} up", lab_hash=self.lab.hash)
