def _add_or_replace_callback(callbacks, method, priority):
    for p, cbs in callbacks.items():
        if priority == p:
            for i, cb in enumerate(cbs):
                if cb.__name__ == method.__name__:
                    callbacks[p][i] = method

            return

    if priority not in callbacks:
        callbacks[priority] = []

    callbacks[priority].append(method)


def before_start_routing(test, priority=10):
    def decorator(method):
        _add_or_replace_callback(test.callbacks['before_start_routing'], method, priority)
        return method

    return decorator


def before_convergence(test, priority=10):
    def decorator(method):
        _add_or_replace_callback(test.callbacks['before_convergence'], method, priority)
        return method

    return decorator


def after_convergence(test, priority=10):
    def decorator(method):
        _add_or_replace_callback(test.callbacks['after_convergence'], method, priority)
        return method

    return decorator


def on_complete(test, priority=10):
    def decorator(method):
        _add_or_replace_callback(test.callbacks['on_complete'], method, priority)
        return method

    return decorator
