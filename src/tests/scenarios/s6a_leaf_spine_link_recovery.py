from tests.link.link_recovery_test import LinkRecoveryTest
from tests.scenarios.s5a_leaf_spine_link_failure import LeafSpineLinkFailure


class LeafSpineLinkRecovery(LeafSpineLinkFailure, LinkRecoveryTest):
    pass
