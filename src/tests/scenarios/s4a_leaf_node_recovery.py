from tests.machine.machine_recovery_test import MachineRecoveryTest
from tests.scenarios.s3a_leaf_node_failure import LeafNodeFailure


class LeafNodeRecovery(LeafNodeFailure, MachineRecoveryTest):
    pass
