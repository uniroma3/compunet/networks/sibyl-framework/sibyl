import logging

from rt_calculator import routing_table_calculator
from tests.decorators import before_start_routing
from tests.link.link_failure_test import LinkFailureTest


class PartitionedFabric(LinkFailureTest):
    @before_start_routing(LinkFailureTest)
    def get_failure_info(self):
        self.results['failed_machine'] = 'tof_1_2_1'
        # Takes first R interfaces that connect the tof with PoD
        r_factor = self.topology_info['redundancy_factor']
        self.results['failed_interfaces'] = self.machine_interfaces[self.results['failed_machine']][:r_factor]

        logging.info('%s selected to make partitioned fabric...' % self.results['failed_machine'])

    def _expected_routing_tables(self, desired_machines):
        return routing_table_calculator.get_routing_tables_in_partitioned_fabric(
            desired_machines, self.topology_info, self.results['failed_machine'], self.results['failed_interfaces']
        )
