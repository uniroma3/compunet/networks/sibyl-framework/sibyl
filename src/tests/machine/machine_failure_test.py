import logging
import os
from abc import ABC

from common.utils import extract_tar_stream
from tests.decorators import after_convergence
from tests.templates.failure_test import FailureTest


class MachineFailureTest(ABC, FailureTest):
    @after_convergence(FailureTest, priority=5)
    def make_failure(self):
        self.agent.stop_dumpers(machine_filter=[self.results['failed_machine']])

        machine_dumps = self.agent.get_dumps(machine_filter=[self.results['failed_machine']])
        shared_directory = os.path.join(self.lab_dir, 'shared')
        extract_tar_stream(machine_dumps, shared_directory)

        self.results['failure_time'] = self.agent.system_times()

        logging.info('Destroying machine %s...' % self.results['failed_machine'])
        self.manager.undeploy_lab(lab_hash=self.lab.hash, selected_machines={self.results['failed_machine']})
