import logging


def compare_routing_tables(real_routing_tables, expected_routing_tables):
    real_nodes = real_routing_tables.keys()
    expected_nodes = expected_routing_tables.keys()

    if len(real_nodes) != len(expected_nodes):
        return False

    converted_routing_tables = {}
    for node, real_routing_table in real_routing_tables.items():
        converted_routing_tables[node] = convert_format(real_routing_table)

    flag = True
    counter = 0
    for node in real_nodes:
        if converted_routing_tables[node] != expected_routing_tables[node]:
            # print(node, '\n', 'Real:\t\t', converted_routing_tables[node], '\n',
            #       'Expected:\t', expected_routing_tables[node]
            #       )
            # print("--------------------------------------------")
            flag = False
        else:
            counter += 1

    logging.info("%d/%d nodes converged!" % (counter, len(real_nodes)))

    return flag


def convert_format(real_routing_table):
    converted_rt = {}

    for entry in real_routing_table:
        if ('200.' in entry['dst'] and '/24' in entry['dst']) or 'default' in entry['dst']:
            subnet = entry['dst']

            if 'nexthops' not in entry:
                converted_rt[subnet] = entry['type'] \
                    if 'type' in entry and entry['type'] in ['unreachable', 'blackhole'] \
                    else 1
            else:
                converted_rt[subnet] = len(entry['nexthops'])

    return converted_rt
