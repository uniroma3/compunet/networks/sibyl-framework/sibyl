#!/usr/bin/python3

import argparse
import sys

from run_test import AVAILABLE_SCENARIOS, run_scenario

parser = argparse.ArgumentParser(description="Run all tests for a specified protocol.")
parser.add_argument('--kube_net', action='store_true', default=False, required=False)
parser.add_argument('--protocol', metavar='PROTOCOL', type=str, required=True,
                    choices=['bgp', 'isis', 'open_fabric', 'rift', 'juniper_rift'])
parser.add_argument('--exclude', metavar='EXCLUDE', default=[], required=False, nargs='*')
parser.add_argument('--topology', metavar='N_RUNS,K_LEAF,K_TOP,R', nargs='+', required=True)

args = parser.parse_args(sys.argv[1:])

params_list = []
for params in args.topology:
    (n_runs, k_leaf, k_top, redundancy_factor) = params.split(',')
    params_list.append((int(n_runs), int(k_top), int(k_leaf), int(redundancy_factor)))

for scenario in AVAILABLE_SCENARIOS:
    if any([x in scenario for x in args.exclude]):
        continue

    for (n_runs, k_leaf, k_top, redundancy_factor) in params_list:
        run_scenario(scenario, args.protocol, n_runs, k_leaf, k_top, redundancy_factor, 0, args.kube_net)
