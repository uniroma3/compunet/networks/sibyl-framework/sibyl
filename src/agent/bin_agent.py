import json
import logging
import os
import shlex
from subprocess import Popen, PIPE, DEVNULL


class BinAgent(object):
    __slots__ = ['lab_path', '_internal_url']

    def __init__(self, lab_path):
        self.lab_path = lab_path
        self._internal_url = None

    def start(self, is_k8s):
        if not is_k8s:
            should_start = self._check_container_status()
            if should_start:
                os.system('docker run -d --env IS_K8S=false --name bin_agent skazza/agent-binaries')
        else:
            os.system('kubectl create -f %s' % os.path.join(self.lab_path, 'agent-binaries.yaml'))
            self._check()
            os.system('kubectl delete -f %s' % os.path.join(self.lab_path, 'agent-binaries.yaml'))

        logging.info("Binaries deployed successfully...")

    @staticmethod
    def _check_container_status():
        command = "docker ps --filter 'status=exited' --filter 'name=bin_agent' -q"
        docker_ps_process = Popen(shlex.split(command), stdout=PIPE, stderr=DEVNULL)
        status = list(map(lambda x: x.decode('utf-8').strip(), docker_ps_process.stdout.readlines()))
        docker_ps_process.terminate()

        # Container is killed, should be restarted.
        if len(status) > 0:
            os.system('docker rm -f %s' % status.pop())
            return True

        command = "docker ps --filter 'status=running' --filter 'name=bin_agent' -q"
        docker_ps_process = Popen(shlex.split(command), stdout=PIPE, stderr=DEVNULL)
        status = list(map(lambda x: x.decode('utf-8').strip(), docker_ps_process.stdout.readlines()))
        docker_ps_process.terminate()

        # Container is running, do nothing.
        if len(status) > 0:
            return False

        # Start the container.
        return True

    @staticmethod
    def _check():
        command = "kubectl get ds agent-binaries -o json"
        ds_process = Popen(shlex.split(command), stdout=PIPE, stderr=DEVNULL)
        ds_info = json.loads("".join(map(lambda x: x.decode('utf-8').strip(), ds_process.stdout.readlines())))
        ds_process.terminate()

        number_desired = ds_info['status']['numberReady']
        number_ready = ds_info['status']['desiredNumberScheduled']

        while number_ready != number_desired or number_desired == 0:
            ds_process = Popen(shlex.split(command), stdout=PIPE, stderr=DEVNULL)
            ds_info = json.loads("".join(map(lambda x: x.decode('utf-8').strip(), ds_process.stdout.readlines())))
            ds_process.terminate()

            number_desired = ds_info['status']['numberReady']
            number_ready = ds_info['status']['desiredNumberScheduled']

    def get_internal_url(self, is_k8s):
        if not self._internal_url:
            self._internal_url = "http://%s" % self._get_relative_url(is_k8s)

        return self._internal_url

    @staticmethod
    def _get_relative_url(is_k8s):
        if not is_k8s:
            command = "docker exec bin_agent /bin/bash -c " + \
                      "\"ip -4 addr show dev eth0 | grep -oP '(?<=inet\\s)\\d+(\\.\\d+){3}'\""
            agent_process = Popen(shlex.split(command), stdout=PIPE, stderr=DEVNULL)
            agent_output = agent_process.stdout.readline().decode('utf-8')
            agent_process.terminate()

            return agent_output.strip()
        else:
            return 'agent-binaries'
