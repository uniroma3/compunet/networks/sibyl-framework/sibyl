import json
import logging
import os
import shlex
from subprocess import Popen, PIPE, DEVNULL

from Kathara.exceptions import MachineNotFoundError
from Kathara.manager.Kathara import Kathara
from Kathara.parser.netkit.LabParser import LabParser


class ControllerAgent(object):
    __slots__ = ['manager', 'lab', 'host_url', 'lab_hash', '_internal_url']

    def __init__(self, lab_path):
        self.manager = Kathara.get_instance()
        self.lab = LabParser.parse(lab_path)
        self.host_url = None

        self._internal_url = None

    def check_and_start(self, is_k8s):
        try:
            self.manager.get_machine_api_object('controller_agent', lab_hash=self.lab.hash)

        except MachineNotFoundError:
            logging.info("Controller test agent not running, starting...")
            self.manager.deploy_lab(self.lab, selected_machines={'controller_agent'})
            logging.info("Controller test agent started successfully...")
            
        if is_k8s:
            self._check_and_start_service()
            agent_api_object = self.manager.get_machine_api_object('controller_agent', lab_hash=self.lab.hash)
            self.host_url = 'http://%s:30000' % agent_api_object.spec.node_name
        else:
            self.host_url = 'http://localhost:3000'

        logging.info("Controller agent URL is %s" % self.host_url)

    def _check_and_start_service(self):
        command = "kubectl -n %s get service controller-agent" % self.lab.hash.lower()
        process = Popen(shlex.split(command), stdout=PIPE, stderr=DEVNULL)
        output = process.stdout.readlines()
        process.terminate()

        # Check if service is started (one line is header, other line is service)
        if len(output) < 2:
            os.system('cat %s/agent-controller.yaml | sed "s/__NAMESPACE__/%s/g" | kubectl create -f -' %
                      (self.lab.path, self.lab.hash)
                      )

    def get_internal_url(self, is_k8s):
        if not self._internal_url:
            self._internal_url = "http://%s:3000" % self._get_relative_url(is_k8s)

        return self._internal_url

    def _get_relative_url(self, is_k8s):
        if not is_k8s:
            exec_output = self.manager.exec('controller_agent', 'ip -j address show dev eth0', lab_hash=self.lab.hash)
            output = ""
            try:
                while True:
                    (stdout, _) = next(exec_output)
                    output += stdout.decode('utf-8') if stdout else ""
            except StopIteration:
                pass

            interface_info = list(filter(lambda x: x, json.loads(output)))[0]
            ipv4_address = [z for z in interface_info["addr_info"] if z['family'] == "inet"][0]
            return ipv4_address['local']
        else:
            return 'controller-agent'
